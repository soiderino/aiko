## 🌹 Start

1. `yarn install`
2. `yarn start`

## 🌹 API

`http://127.0.0.1/` <br>
`http://127.0.0.1/upload/`

## 🌹 DOCS

https://fastify.dev/docs/latest/Reference/
